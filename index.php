<?php
include 'settings.php';

$table = 'graduation';
$uri = 'https://graduation.rubenmelchers.nl/';

$query = "SELECT * FROM " . $table;
$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
$method = $_SERVER["REQUEST_METHOD"];
$accept = $_SERVER["HTTP_ACCEPT"];
switch ($method) {
    case "GET":
        if ($accept == "application/json" || $accept == "Application/json" ) {
            header("Content-Type: application/json");

            /** Empty arrays for pushing all content **/
            $allContent = [];
            $items = [];
            $links = [];
            $pagination = [];
            $detail = [];

            /** Returning SQL data in JSON format with/without id / limit */
            /** WITH LIMIT */
            if(isset($_GET['limit'])){
                $limit = $_GET['limit'];
                if(isset($_get['id'])){
                    $id = $_GET['id'];
                    $limitQuery = "SELECT * FROM " . $table . " LIMIT " . $limit . " WHERE id=" . $id;
                    $limResult = mysqli_query($conn, $limitQuery) or die(mysqli_error($conn));
                }
                else{
                    $limitQuery = "SELECT * FROM " . $table . " LIMIT " . $limit;
                    $limResult = mysqli_query($conn, $limitQuery) or die(mysqli_error($conn));
                }

                while($row = mysqli_fetch_assoc($limResult)){
                    /** als de title leeg is, vul het dan met een standaard title */
                    if($row['title'] == "")
                    {
                        $title = 'no title';
                    } else
                    {
                        $title = $row['title'];
                    }

                    /** Push all mysql content to $items array */
                    array_push($items, array(
                        'id' => $row['id'],
                        'title' => $title,
                        'slide' => $row['slide'],
                        'isActive' => $row['isactive'],
                        'links' => array(
                            array(
                                'rel' => 'self',
                                'href' => $uri
                            ),
                            array(
                                'rel' => 'collection',
                                'href' => $uri . $row['id']
                            ))
                    ));
                }
            }
            /** WITHOUT LIMIT */
            else{

                /**
                 * If statement for ID (if there is an ID in the link,
                 * show only 1 record with the correct ID
                 */
                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    $query = "SELECT * FROM " . $table . " WHERE id=" . $id;
                    $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                    if(mysqli_num_rows($result) == 0 )
                    {
                        http_response_code(404);
                    }
                }

                /** While loop with data */
                while($row = mysqli_fetch_assoc($result))
                    {
                        /** als de title leeg is, vul het dan met een standaard title */
                        if($row['title'] == "")
                        {
                            $title = 'no title';
                        } else
                        {
                            $title = $row['title'];
                        }

                        $links = array(
                            array(
                                'rel' => 'self',
                                'href' => $uri . $row['id']
                            ),
                            array(
                                'rel' => 'collection',
                                'href' => $uri
                            )
                        );

                        $detail = array(
                            'id' => $row['id'],
                            'title' => $title,
                            'slide' => $row['slide'],
                            'isActive' => $row['isActive'],
                            'links' => array(
                                array(
                                    'rel' => 'self',
                                    'href' => $uri . $row['id']
                                ),
                                array(
                                    'rel' => 'collection',
                                    'href' => $uri
                                ))
                        );

                        /** Push all mysql content to $items array */
                        array_push($items, array(
                            'id' => $row['id'],
                            'title' => $title,
                            'slide' => $row['slide'],
                            'isActive' => $row['isActive'],
                            'links' => array(
                                array(
                                    'rel' => 'self',
                                    'href' => $uri . $row['id']
                                ),
                                array(
                                    'rel' => 'collection',
                                    'href' => $uri
                                ))
                        ));
                    }
            }

            /** Links array part */
            $links = array (
                array(
                    'rel' => 'self',
                    'href' => $uri
                )
            );

            /** pagination part */
            $totalItems = mysqli_num_rows($result);
            if(isset($_GET['limit'])){
                $limit = $_GET['limit'];
            }else{
                $limit = $totalItems;
            }
            $pages = ceil($totalItems / $limit);
            $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
                'options' => array(
                    'default'   => 1,
                    'min_range' => 1,
                ),
            )));
            $offset = ($page - 1) * $limit;
            $start = $offset + 1;
            $end = min(($offset + $limit), $totalItems);

            if(isset($_GET['start'])){
                $start = $_GET['start'];
                $page = ceil($start / $limit);

                if($page == $pages){
                    $next = $pages;
                    $nextStart = ($pages - 1) * $limit + 1;
                }else{
                    $next = $page + 1;
                    $nextStart = $start + $limit;
                }
                if($page - 1 == 0){
                    $previous = 1;
                    $prevStart = 1;
                }else{
                    $previous = $page - 1;
                    $prevStart = $start - $limit;
                }
            }
            else{
                $nextStart = $limit + 1;
                if($page == $pages){
                    $next = $pages;
                }else{
                    $next = $page + 1;
                }
                $prevStart = 1;
                if($page == 1){
                    $previous = 1;
                }else{
                    $previous = $page - 1;
                }
            }

            /** Pagination links array */
            if(isset($_GET['limit'])){
                $last = ($pages - 1) * $limit + 1;
                $paginationLinks = [
                    array(
                        'rel' => 'first',
                        'page' => '1',
                        'href' => $uri . '?start=1&limit=' . $limit
                    ),
                    array(
                        'rel' => 'last',
                        'page' => $pages,
                        'href' => $uri . '?start=' . $last . '&limit=' . $limit
                    ),
                    array(
                        'rel' => 'previous',
                        'page' => $previous,
                        'href' => $uri . '?start=' . $prevStart  . '&limit=' . $limit
                    ),
                    array(
                        'rel' => 'next',
                        'page' => $next,
                        'href' => $uri . '?start=' . $nextStart  . '&limit=' . $limit
                    )
                ];
            }else{
                $paginationLinks = [
                    array(
                        'rel' => 'first',
                        'page' => '1',
                        'href' => $uri
                    ),
                    array(
                        'rel' => 'last',
                        'page' => $pages,
                        'href' => $uri
                    ),
                    array(
                        'rel' => 'previous',
                        'page' => $previous,
                        'href' => $uri
                    ),
                    array(
                        'rel' => 'next',
                        'page' => $next,
                        'href' => $uri
                    )
                ];
            }

            $pagination = [];
            $pagination['currentPage'] = $page;
            $pagination['currentItems'] = $end;
            $pagination['totalPages'] = $pages;
            $pagination['totalItems'] = $totalItems;
            $pagination['links'] = $paginationLinks;

            /** Push all arrays to the $allContent array */
            if(isset($_GET['id']))
            {
                echo json_encode($detail);
            }
            else{
                $allContent['slides'] = $items;
                $allContent['links'] = $links;
                $allContent['pagination'] = $pagination;
                echo json_encode($allContent);
            }
            mysqli_close($conn);
          }
        /** XML part */
        else if($accept == "application/xml") {
            header("Content-Type: application/xml");
            $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                    $xml .= "<slides>";
            while($data = mysqli_fetch_assoc($result)) {
                $xml .= "<slide>";
                foreach($data as $key => $value) {
                    $xml .= "<$key>";
                    $xml .= "$value";
                    $xml .= "</$key>";
                    }
                $xml .= "</slide>";
                }
            $xml .= "</slides>";
            echo $xml;
            }
        else {
            http_response_code(406);
            }
        break;
    case "POST":

        /**
         * If JSON is posted to the api. Insert the data into the database
         */
        if($_SERVER["CONTENT_TYPE"] == "application/json"){
            header("Content-type: application/json");
            http_response_code(201);
            $data = json_decode(file_get_contents("php://input"), true);
            $fields = array(
                "title",
                "slide",
                "isActive");

            foreach($fields as $field){
                if(!isset($data[$field]) || empty($data[$field])){
                    http_response_code(400);
                    echo $field . " is empty";
                    exit;
                }
            }
            $title = $data['title'];
            $slide = $data['slide'];
            $isActive = $data['isActive'];

            $sql = "INSERT INTO " . $table . " (id, title, slide, isActive) VALUES (
                'NULL',
                '{$data['title']}',
                '{$data['slide']}',
                '{$data['isActive']}',";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
            mysqli_close($conn);
            die();
            }
        /**
         * If the post is empty, return the 400 response code or if it isn't empty, insert the POST data into
         * the database.
         **/
        else if(empty($_POST)){
            die(http_response_code(400));
        }
        else if(!empty($_POST)){
            header("Content-type: application/x-www-form-urlencoded");
            http_response_code(201);

            $title = $_POST['title'];
            $slide = $_POST['slide'];
            $isActive = $_POST['isActive'];

            $postQuery = "INSERT INTO " . $table . " (title, slide, isActive) VALUES (
            '" . $title . "',
            '". $slide . "',
            '". $isActive . "')";
            $postResult = mysqli_query($conn, $postQuery) or die(mysqli_error($conn));

            $lastId = mysqli_insert_id($conn);
            header("Location: " . $uri . $lastId);
            mysqli_close($conn);
            die();

        }
        break;
    case "PUT":
        if(isset($_GET['id'])) {
            header("Content-type: application/json");
            http_response_code(200);
            $data = json_decode(file_get_contents("php://input"), true);
            $fields = array("title", "slide", "isActive");
            foreach($fields as $field){
                if(!isset($data[$field]) || empty($data[$field])){
                    http_response_code(400);
                    echo $field . " is empty";
                    exit;
                }
            }
            $id = $_GET['id'];
            $sql = "UPDATE graduation SET title= '{$data['title']}', slide='{$data['slide']}', isActive='{$data['isActive']}' WHERE id = $id";
            mysqli_query($conn, $sql) or die(mysqli_error($conn));
            http_response_code(200);
            mysqli_close($conn);
        }
        else{
            /** 405 = method not allowed */
            http_response_code(405);
        }
        break;
    case "DELETE":
        if(isset($_GET['id']))
        {
            $id = $_GET['id'];
            $deleteQuery = "DELETE FROM " . $table . " WHERE id=" . $id;
            $checkQuery = "SELECT * FROM " . $table . " WHERE id=" . $id;
            $check = mysqli_query($conn, $checkQuery) or die(mysqli_error($conn));
            if(mysqli_num_rows($check) == 0 )
            {
                http_response_code(404);
            }
            else{
                http_response_code(204);
                $delete = mysqli_query($conn, $deleteQuery) or die(mysqli_error($conn));
                mysqli_close($conn);
            }
        }
        else{
                http_response_code(405);
        }
        break;
    case "OPTIONS";
        /**
         * if a single resource is requested, the options are different than
         * from the whole collection.
         * single resources can be deleted.
         */
        if(isset($_GET['id'])){
            header("ALLOW: GET,PUT,DELETE,OPTIONS");
        }
        else {
            header("Allow: GET,POST,OPTIONS");
        }
        break;
    default:
        echo "Choose another request method";
        break;
}
